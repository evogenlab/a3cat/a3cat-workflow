import logging
import os
from ete3 import NCBITaxa
from utils import setup_logging, create_dir

# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    logging.info('Retrieving information from Snakemake')

    # Get parameters from snakemake object
    db_file_path = snakemake.params.db_path

    logging.info('Initializing ete3 database')
    DB_PATH = os.path.realpath(db_file_path)
    create_dir(os.path.dirname(db_file_path))
    # Create an empty db file if first time running the workflow
    if not os.path.isfile(db_file_path):
        with open(db_file_path, 'w') as dbfile:
            logging.info(f'Initialized empty ete3 db file at <{db_file_path}>')
    ncbi = NCBITaxa(dbfile=db_file_path)
    # Remove taxa dump tarball if not cleaned up
    if os.path.isfile('taxdump.tar.gz'):
        os.remove('taxdump.tar.gz')
    logging.info('Successfully downloaded ete3 database')
