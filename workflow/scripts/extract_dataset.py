import json
import logging
import os
import subprocess
from utils import setup_logging, save_dict_to_json, json_object_hook


# Main script execution
if __name__ == '__main__':

    setup_logging(snakemake)

    # Retrieve info from snakemake
    logging.info('Getting data from Snakemake')
    tmp_download_filename = snakemake.input[0]
    accession = snakemake.wildcards.accession
    output_file_path = snakemake.output.assembly
    info_file_path = snakemake.output.info

    # Create a temporary directory for the dataset download
    tmp_dir = f"{tmp_download_filename}_extract"
    os.makedirs(tmp_dir, exist_ok=True)

    # Download dataset using datasets CLI
    logging.info(f'Downloading dataset for accession {accession}')
    download_cmd = [
        'datasets', 'download', 'genome', 'accession', accession,
        '--include', 'genome,data-report',
        '--filename', f'{tmp_download_filename}'
    ]
    result = subprocess.run(download_cmd, capture_output=True, text=True)
    if result.returncode != 0:
        raise RuntimeError(f"datasets download failed: {result.stderr}")

    # Extract the downloaded zip file
    logging.info('Extracting downloaded dataset')
    extract_cmd = ['unzip', '-q', '-o', tmp_download_filename, '-d', tmp_dir]
    result = subprocess.run(extract_cmd, capture_output=True, text=True)
    if result.returncode != 0:
        raise RuntimeError(f"unzip failed: {result.stderr}")

    # Read the data report
    logging.info(f'Saving assembly info to {info_file_path}')
    data_report_path = os.path.join(tmp_dir, 'ncbi_dataset', 'data', 'assembly_data_report.jsonl')
    if not os.path.exists(data_report_path):
        raise RuntimeError(f"Could not find data report at {data_report_path}")
    
    with open(data_report_path) as f:
        report = json.load(f, object_hook=json_object_hook)
    save_dict_to_json(info_file_path, report)

    # Copy the genome FASTA file
    logging.info(f'Saving assembly sequence to {output_file_path}')
    fasta_path = os.path.join(tmp_dir, 'ncbi_dataset', 'data', accession, f'{accession}_genomic.fna')
    if not os.path.exists(fasta_path):
        raise RuntimeError(f"Could not find genome FASTA at {fasta_path}")
    
    with open(fasta_path, 'r') as input_file, open(output_file_path, 'w') as output_file:
        output_file.write(input_file.read())

    # Clean up temporary directory
    subprocess.run(['rm', '-rf', tmp_dir])

    logging.info(f'Successfully downloaded dataset <{accession}>')
